# welcome to hell

This is my journey through customizing my laptop to work exactly the way I want it to be, across my various PCs, laptops, and even some servers. This current iteration of the guide is from my XPS 15 9500, bought in 2020. My older devices (in chronological order) are listed below. I want this to serve as a tracker of my Linux journey, since I was 13 years old with my first PC running Ubuntu. As I game, all my machines are dual-booted. Yes, I play on Linux whenever I can, Windows is only for Destiny 2.

- [2014] AMD FX 6300+Radeon HD7850 2GB, 8GB RAM, ASUS M5A78LM-usb3 mobo - Ubuntu 14.04 + Windows 8/10
- [2016] Dell XPS 15 9550 (i7, 1080p) - Ubuntu 16.04 + Windows 10
- [2018] AMD Ryzen 1700X + GTX 1060 - Ubuntu 16.04 + Windows 10
- [2020] AMD Ryzen 2600 + GTX 1660 - Ubuntu 16.04 + Windows 10
- [2020] Dell XPS 15 9500 (2020) - Ubuntu 20.04 + Windows 10
	- We are here!

I still retain the original boot drives for all of these machines except the 2014 PC. I plan on diving into their unique quirks and their relevant fuckups in other branches of this repo.

idk if you ever stumble upon this, but if you do, good luck. not sure what help this might be of you in your linux journey.

# XPS 15 9500

## Ubuntu 20.04 with Unity
Wait, what? Unity in the 21st century? Yes. I like this over GNOME, I like this over KDE Plasma, I like it over i3. It fits my [workflow](https://xkcd.com/1172/), ok?

## Desktop/Navigation

I have 3x3 Workspaces setup within Unity, and I segregate them for various apps. Generally, it's something along the lines of -
- Chrome personal
- Chrome work
- Slack
- VSCode
- Discord
- Music app (Spotify or YTMusic, I use both)

I do not have a dedicated workspace for my terminal, because I use the best terminal on the planet - [Guake](http://guake-project.org/). It drops down on any workspace on F12, and always has all my tabs available. It now also has built-in splitting of windows, and it is the best workflow tool I have.

## Trackpad Gestures
I used to use synaptics gestures, but am now on the much more convenient [libinput-gestures](https://github.com/bulletmark/libinput-gestures). XPSs generally have had the best trackpad on the market, and I love performing gestures on them.

I have a slew of trackpad gestures that I use to make life much easier -
- **2finger tap** - right click
- **3finger tap** - middle click. also alt clipboard in linux.
- **2finger up/down**: scroll
- **3finger left/right**: Ctrl+PgUp, Ctrl+PgDwn - tab change left and right. this is better than ctrl+shift+tab or ctrl+shift+tab because it works in more apps. 
- **3finger up**: spread windows
- **4finger swipe**: change workspace up/down/left/right - 2D workspaces is the biggest reason I love Unity.

And that's a better set of gestures than a Mac already.

My config file is [here](libinput-gestures.conf).

## Batter/Power Management

GTX1650 - Runs in [Nvidia's On-Demand mode](https://itectec.com/ubuntu/ubuntu-how-nvidia-on-demand-option-works-in-nvidia-x-server-settings/) and it does a great job for me.
i7-10750H - I use auto-cpufreq and TLP. I'll share the configs at some point.

## Fingerprint Reader Drivers

[Dell has official repos for drivers!](https://www.dell.com/community/XPS/XPS-13-9300-Does-fingerprint-reader-work-on-linux/m-p/7628310/highlight/true#M63982)

## IR and Face Recognition

I use Howdy with the Official Dell IR drivers, linked above


